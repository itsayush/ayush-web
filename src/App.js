import "./App.css";

// import axios from "axios";
import React, { useEffect, useState } from "react";

import Skeleton from "./components/Skeleton";
import LazyImage from "./components/LazyImage";

const App=()=> {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  
  // const [image, setImage] = useState(false);

  // useEffect(() => {
  //   setIsLoading(true);

  //   // Intentionally delay the function execution
  //   new Promise(res => {
  //     setTimeout(() => {
  //       res();
  //     }, 3000);
  //   }).then(() => {
  //     axios.get("https://reqres.in/api/users?page=2").then(res => {
  //       setData(res.data.data);
  //       setTimeout(() => setIsLoading(false), 2000);
  //     });
  //   });
  // }, []);

  return (
    <div className="App">
    {/* <div>
    <LazyImage
      imageSource={'https://upload.wikimedia.org/wikipedia/commons/f/ff/Pizigani_1367_Chart_10MB.jpg'}
      imageAltText={''}
      height={400}
      width={400}
      />
    </div> */}
    
     
      <Skeleton />
       
      
    </div>
  );
}

export default App;

import "./Skeleton.css";

import React from "react";

const Skeleton = () => {
  return (
    <div className="skeleton-item">
    <img src={'https://timesascent.com/images/cms/articles/2021/8/13/article_default_large_1628861200583.png'}
        alt={'Loading ...'}
        className="imageSkeleton"
        />
        
      <div className="container">
        <div className="skeleton-img" />
        
      
      <div className="skeleton-info">
        <p className="skeleton-name" />
        <p className="skeleton-email" />
      </div>
      <div className="footer">
        <div className="skeleton-footer" />
        <div className="skeleton-footer" />
      </div>
      </div>
    </div>
  );
};

export default Skeleton;

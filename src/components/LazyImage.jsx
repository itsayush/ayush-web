import React,{ useEffect, useState } from 'react'
import "./LazyImage.css"

const LazyImage=(props)=> {

    const [loaded, setloaded] = useState(false);
    return (
        <div className="mainContainer">
        <div className="loaderContainer" style={{height: props.height,width: props.width }}>
        
        <div class="loader"></div>
        </div>
            {/* <img
                className={"image"}
                src={props.imageSource}
                alt={props.imageAltText}
                style={loaded ? {} : {display: 'none'}}
                onLoad={()=>setloaded(true)}
                /> */}
        </div>
    )
}

export default LazyImage
